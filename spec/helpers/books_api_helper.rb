require 'plissken'

module BooksApiHelper
  def self.volume_from_file(filename)
    volume_args_hash = args_from_books_api_json(filename)
    Google::Apis::BooksV1::Volume.new(prepare_volume_args(volume_args_hash))
  end

  def self.volumes_from_file(filename)
    volumes_args_hash = args_from_books_api_json(filename)

    volumes_args_hash[:items].map! do |volume_args_hash|
      Google::Apis::BooksV1::Volume.new(prepare_volume_args(volume_args_hash))
    end

    Google::Apis::BooksV1::Volumes.new(volumes_args_hash)
  end

  private

  def self.args_from_books_api_json(filename)
    JSON.parse(File.read(filename)).to_snake_keys.deep_symbolize_keys
  end

  def self.prepare_volume_args(volume_args_hash)
    image_links = Google::Apis::BooksV1::Volume::VolumeInfo::ImageLinks.new(volume_args_hash[:volume_info][:image_links])
    volume_args_hash[:volume_info][:image_links] = image_links

    volume_info = Google::Apis::BooksV1::Volume::VolumeInfo.new(volume_args_hash[:volume_info])
    volume_args_hash[:volume_info] = volume_info

    volume_args_hash
  end
end
