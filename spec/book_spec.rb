require 'spec_helper'

describe Book do
  let(:api_response_filename) { "spec/support/google_books_still_life_with_woodpecker.json" }
  let(:volume) { BooksApiHelper.volume_from_file(api_response_filename) }
  let(:book) { Book.new(volume) }

  describe "#initialize" do
    it "correctly parses the title" do
      expect(book.title).to eq("Still Life With Woodpecker")
    end

    it "correctly parses the publisher" do
      expect(book.publisher).to eq("Bantam Books")
    end

    it "correctly parses the image_url" do
      expect(book.image_url).to eq("http://books.google.com/books/content?id=vfhvDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE72vZkVtMPSZ2fjz5TiscXjUrHtp1-GNQHlj6tG54j7Ucdpq1qwYJlD2rh2Z5Smz6iC-WsdX1_f-CERkIp4sDsxFjvLydBpEQBI9oy8b2EnAdnpg9zIX4BcK_yAkSpbMpt1aBofK&source=gbs_api")
    end

    it "correctly parses the info_url" do
      expect(book.info_url).to eq("https://play.google.com/store/books/details?id=vfhvDwAAQBAJ&source=gbs_api")
    end

    it "correctly parses the author as an array" do
      expect(book.authors).to eq(["Tom Robbins"])
    end

    context "when the book has more than one author" do
      let(:api_response_filename) { "spec/support/google_books_good_omens.json" }

      it "correctly parses all of the authors as an array" do
        expect(book.authors).to match(["Neil Gaiman", "Terry Pratchett"])
      end
    end

    context "when the API response does not include all fields" do
      before(:each) do
        volume.volume_info.title = nil
        volume.volume_info.authors = nil
        volume.volume_info.info_link = nil
        volume.volume_info.image_links = nil
        volume.volume_info.publisher = nil
      end

      it "assigns the attributes for all missing fields as nil" do
        expect(book.title).to be_nil
        expect(book.authors).to be_nil
        expect(book.info_url).to be_nil
        expect(book.image_url).to be_nil
        expect(book.publisher).to be_nil
      end
    end
  end
end
