describe BookDecorator do
  let(:volume) { Google::Apis::BooksV1::Volume.new(volume_info: volume_info) }
  let(:volume_info) { Google::Apis::BooksV1::Volume::VolumeInfo.new(title: title, authors: authors, info_link: info_url, publisher: publisher, image_links: image_links) }
  let(:image_links) { Google::Apis::BooksV1::Volume::VolumeInfo::ImageLinks.new(image_links_options) }

  let(:image_links_options) { {thumbnail: "http://books.google.com/this_books_image_thumbnail"} }
  let(:publisher) { "Acme Books" }
  let(:title) { "My Cool Title" }
  let(:authors) { ["Sylvia Plath"] }
  let(:info_url) { "http://books.google.com/info_on_this_book" }

  let(:book) { Book.new(volume) }
  let(:decorated_book) { BookDecorator.new(book) }

  describe "#authors" do
    it "returns a string listing the author" do
      expect(decorated_book.authors).to eq("Author: Sylvia Plath")
    end

    context "when more than one author" do
      let(:authors) { ["Sylvia Plath", "Denis Johnson", "Matthew Dickman"] }

      it "returns a string listing the authors" do
        expect(decorated_book.authors).to eq("Authors: Sylvia Plath, Denis Johnson, Matthew Dickman")
      end
    end

    context "when book's author is nil" do
      let(:authors) { nil }

      it "lists the author as unknown" do
        expect(decorated_book.authors).to eq("Author: Unknown")
      end
    end

    context "when book's author is an empty array" do
      let(:authors) { [] }

      it "lists the author as unknown" do
        expect(decorated_book.authors).to eq("Author: Unknown")
      end
    end
  end

  describe "#title" do
    it "returns the book's title when one is present" do
      expect(decorated_book.title).to eq("My Cool Title")
    end

    context "when book's title is nil" do
      let(:title) { nil }

      it "lists the book's title as unknown, without a colon" do
        expect(decorated_book.title).to eq("Title unknown")
      end
    end
  end

  describe "#publisher" do
    it "returns a string listing the publishera string listing the publisher" do
      expect(decorated_book.publisher).to eq("Publisher: Acme Books")
    end

    context "when book's publisher is nil" do
      let(:publisher) { nil }

      it "lists the book's publisher as unknown" do
        expect(decorated_book.publisher).to eq("Publisher: Unknown")
      end
    end
  end

  describe "#info_link" do
    it "returns an anchor tag targeting the book's info_url" do
      expect(decorated_book.info_link).to eq("<a href=\"http://books.google.com/info_on_this_book\">View on Google Books</a>")
    end

    context "when book's info_url is nil" do
      let(:info_url) { nil }

      it "returns nil" do
        expect(decorated_book.info_link).to be_nil
      end
    end
  end

  describe "#image" do
    it "returns an image tag with the book's image_url" do
      expect(decorated_book.image).to eq("<img src=\"http://books.google.com/this_books_image_thumbnail\">")
    end

    context "when book's image_url is nil" do
      let(:image_links_options) { {thumbnail: nil} }

      it "returns an image-not-found div" do
        expect(decorated_book.image).to eq("<div class=\"image-not-found\"><p>No Image Available</p></div>")
      end
    end
  end
end
