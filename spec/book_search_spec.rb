require 'spec_helper'

describe BookSearch do
  let(:book_search) { BookSearch.new("dylan thomas") }
  let(:api_double) { double("Books API") }
  let(:volumes) { BooksApiHelper.volumes_from_file("spec/support/google_books_dylan_thomas.json") }

  before(:each) do
    allow(api_double).to receive(:list_volumes).with("dylan thomas").and_return(volumes)
    allow_any_instance_of(BookSearch).to receive(:books_api).and_return(api_double)
  end

  describe "#initialize" do
    it "sets the books attribute as an array of Book objects" do
      expect(book_search.books).to all(be_a(Book))
    end

    it "sets the books attribute to contain all volumes returned by the books API" do
      expect(book_search.books.map {|book| book.title}).to match_array([
        "Under Milk Wood",
        "The Collected Poems of Dylan Thomas: The Original Edition",
        "The Poems of Dylan Thomas",
        "Dylan Thomas",
        "Adventures in the Skin Trade",
        "On the Air with Dylan Thomas",
        "Dylan Thomas",
        "The Collected Stories",
        "The Love Letters of Dylan Thomas",
        "Dylan Thomas",
      ])
    end
  end
end
