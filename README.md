# Book Finder -- An 8th Light Code Challenge Submission

This code challenge was written using Ruby 2.4.5, and should be compatible with any version of Ruby 2.0.

## Running tests

Before running the tests, you'll want to use bundler to grab its dependencies. You can do so on the command
line by running `bundle` from the root directory of the project.

If you do not have bundler installed, you can do so by running `gem install bundler`.

Once you're bundled, simply run `rake` to run the test suite.

## Running locally

Follow the bundling instructions above. You can then run `rackup` to run the application, which will be available at `localhost:9292`.

## Design Notes

I wrote this using Ruby since it's the language I'm most comfortable in. I first had started this as a Rails application, but quickly realized that I didn't need nearly all of the bells and whistles that Rails provides, so I switched over to using Sinatra as it's a much more lightweight framework. YAGNI!

It's been a few years since I've written CSS, and my career focus as of the last few years has been strictly back-end development, so I chose to spend the majority of my time showing you what I can do on the Ruby side of things. The front-end work on this is _very_ minimal, which would definitely be a large space for improvement were I to expand this project.

Some other things I would tackle that didn't make my MVP:

- Making the app a one-pager by having the search form submit via AJAX and having `/search` return the decorated books as JSON
- Paginating the search results (using the API's `start_index` option for `BooksService#list_volumes`) so that users can load additional results.
- Adding an option to choose how many search results you see per page (using the `max_results` option)
- Capybara/cucumber acceptance tests
- Refactoring out partials from the views

Thanks for taking the time to review!
