get '/search' do
  query = params[:query]
  @decorated_books = decorate_books(BookSearch.new(query).books)

  erb :search
end

def decorate_books(books)
  books.map { |book| BookDecorator.new(book) }
end
