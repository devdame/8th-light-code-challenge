require 'google/apis/books_v1'

class Book
  attr_reader :authors, :image_url, :info_url, :publisher, :title

  def initialize(volume)
    @authors = volume.volume_info.authors
    @image_url = volume.volume_info.image_links&.thumbnail
    @info_url = volume.volume_info.info_link
    @publisher = volume.volume_info.publisher
    @title = volume.volume_info.title
  end
end
