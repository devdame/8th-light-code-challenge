class BookDecorator
  def initialize(book)
    @book = book
  end

  def authors
    pluralization = book.authors&.count.to_i > 1 ? "s" : ""
    author_components = ["Author", pluralization, ": "]
    if book.authors.nil? || book.authors.empty?
      author_components << "Unknown"
    else
      author_components << book.authors.join(", ")
    end

    author_components.join
  end

  def title
    book.title || "Title unknown"
  end

  def publisher
    "Publisher: #{book.publisher || "Unknown" }"
  end

  def info_link
    if book.info_url
      "<a href=\"#{book.info_url}\">View on Google Books</a>"
    end
  end

  def image
    if book.image_url
      "<img src=\"#{book.image_url}\">"
    else
      "<div class=\"image-not-found\"><p>No Image Available</p></div>"
    end
  end

  private

  attr_reader :book
end
