require 'google/apis/books_v1'

class BookSearch
  attr_reader :books

  def initialize(query)
    @books = parse_volumes(books_api.list_volumes(query))
  end

  private

  def parse_volumes(volumes)
    volumes.items.map {|volume| Book.new(volume)}
  end

  def books_api
    Google::Apis::BooksV1::BooksService.new
  end
end
